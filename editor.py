#!/usr/bin/env python3

# may god have mercy on your soul if you read this code

import curses
from time import sleep
from traceback import print_exception
import editor

# initialization
screen = curses.initscr()

curses.start_color()
curses.use_default_colors()
curses.noecho()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
esc_delay = curses.get_escdelay()
curses.set_escdelay(25)

try:
    curses.curs_set(0)

except:
    print("warning: invisible cursor not supported!")

excep = None

try:
    top = screen.subwin(1, screen.getmaxyx()[1], 0, 0)
    mid = screen.subwin(screen.getmaxyx()[0] - 2, screen.getmaxyx()[1], 1, 0)
    bot = screen.subwin(1, screen.getmaxyx()[1], screen.getmaxyx()[0] - 1, 0)
    win = editor.Windows(top, mid, bot)
    editor.load_entries(mid)
    menu = editor.EntriesMenu(win)
    menu.loop()

except Exception as e:
    # handle exception because abruptly exiting curses causes problems
    excep = e

# exiting
curses.nocbreak()
screen.keypad(False)
curses.echo()
curses.set_escdelay(esc_delay)

curses.endwin()

if excep:
    print("Something went horribly wrong...")
    print_exception(excep)
