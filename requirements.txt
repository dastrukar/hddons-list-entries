markdown==3.4.3
pymdown-extensions==9.9
pymarkdown-video @ git+https://github.com/allesblinkt/pymarkdown-video.git@9fee1b7
ruamel.yaml==0.17.21
pyperclip==1.8.2
