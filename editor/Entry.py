class Entry:
    def __init__(self, contents: dict, filename: str):
        self.title = contents["title"] if "title" in contents.keys() else ""
        self.credits = contents["credits"] if "credits" in contents.keys() else ""
        self.dependencies = contents["dependencies"] if "dependencies" in contents.keys() else []
        self.tags = contents["tags"] if "tags" in contents.keys() else []
        self.flairs = contents["flairs"] if "flairs" in contents.keys() else {}
        self.flag = contents["flag"] if "flag" in contents.keys() else ""
        self.description = contents["description"] if "description" in contents.keys() else ""
        self.filename = filename

    def save_entry(self) -> None:
        # filename = replace_unwanted_chars(self.title) + ' - ' + replace_unwanted_chars(self.credits)
        with open(self.filename, 'w') as file:
            to_write = "title: \"{}\"\n".format(self.title.replace('"', '\\"'))
            to_write += "credits: \"{}\"\n".format(self.credits.replace('"', '\\"'))
            if len(self.dependencies) > 0:
                to_write += "dependencies:\n"
                for i in self.dependencies:
                    to_write += "    - \"{}\"\n".format(i.replace('"', '\\"'))

            if len(self.tags) > 0:
                to_write += "tags:\n"
                for i in self.tags:
                    to_write += "    - \"{}\"\n".format(i.replace('"', '\\"'))

            if len(self.flairs) > 0:
                to_write += "flairs:\n"
                for i in self.flairs.keys():
                    to_write += "    \"{}\": \"{}\"\n".format(i.replace('"', '\\"'), self.flairs[i].replace('"', '\\"'))

            if len(self.flag) > 0:
                to_write += "flag: \"{}\"\n".format(self.flag)

            to_write += "description: |\n"
            to_write += "    {}".format(self.description.replace('\n', '\n    ').replace('    \n', '\n'))
            if to_write[-1] != '\n':
                to_write += '\n'
                to_write = to_write.replace('    \n', '')

            file.write(to_write)
