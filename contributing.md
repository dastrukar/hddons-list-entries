## Prerequisites
- Python 3.X (this is only if you want to use the `new_entry.py` tool)

## Rules

### Adding categories
- A new category may only be added if there are a bunch of addons that only fit in the new category, and are categorised under the `misc` category.

### Removing categories
- A category should only be removed when...
    * ...there are no/too little (1-2) addons in the category.
    * ...when all the addons are no longer usable for any reason.
- Should you consider to remove a category that still has any entries in it, but don't want to remove the entries. You can simply remove access to the category from the website.

### Adding entries
- You may only add addons...
    * ...posted in the `#mod-releases` channel in the Hideous Destructor Discord Server.
    * ...with permission from the addon author themselves.
- On a side-note, do refrain from adding addons that aren't related to Hideous Destructor.

### Removing entries
- Entries can only be removed if the author requests for it.


## Adding new entries
### Creating the YAML file
To create a new entry, run `new_entry.py` and fill in the details.   
After that it should automatically generate a new entry file for you.

If you don't want to use `new_entry.py`, you can just manually copy `template.yaml` and paste it into a subdirectory/category. (please make sure the filename uses the correct format)

### Filling in the rest
Once you've created the entry's YAML file, open up your preferred text editor and edit the file.

If you need help with the syntax, please refer to the `readme.md`.

If you still feel lost after reading all of the notes given below, you can always look at the existing entries as an example.

Anyways, here's some notes:

- Variables are **case-sensitive**. Please make sure they are all in lowercase.
- Indent size is 4 spaces.
- Make sure to remove any unused variables!

`title` & `credits`:
- Self-explanatory.
- If you are adding from `#mod-releases`, *always* use the title provided in the post.
- In the case that there was no title provided, you may come up with a name for the entry.

`tags`:
- Only applies to the `skins` category.
- You may only add a new tag if it makes the entries easier to sort through.

`flairs`:
- Make sure to link any requirements. You may also link to addons that aren't on the website if needed.
- For types of flairs that are available, please refer to the `readme.md`.

`flag`:
- Only use a flag if applicable. (you probably won't use this)
- For types of flags that are available, please refer to the `readme.md`.

`description`:
- If you are adding from `#mod-releases`, try to keep the description accurate to the original post. You should only make modifications if...
    * ...there are errors in the description. (e.g: grammar errors, spelling errors, etc.)
    * ...you wish to improve the look of the description. (e.g: adding bold text, italics, collapsables, etc.)
- If there's no provided description, you can...
    * ...make up your own description for the entry. (provided the author is fine with it)
    * ...just put "(no description provided)".
- Only use collapsables if...
    * ...there's a large amount of text that makes the entry very big on the website.
    * ...there are more than one iconlink.
- Also, make sure the text is indented properly.

### Testing out your entry
If you're not sure if your new addon entry works, you can always just run `compile.py` and use the [preview page on the website.](https://dastrukar.gitlab.io/hddons-list/preview.html)

## Removing entries
To remove an entry, just delete the yaml file and it'll be automatically removed on the next CI pipeline.
